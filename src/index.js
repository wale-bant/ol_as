import './scss/style.scss';

const sections = document.querySelectorAll('.section')
const sectionInfo = document.querySelector('.section__info');
const navLink = document.querySelectorAll('.header__navbar-links')
const editAllFields = document.querySelector('#mobile-edit')
const form = document.querySelector('#form')
const cancel = document.querySelector('#cancel')
const save = document.querySelector('#submit')

// mobile form logic
const showAbout = () => {
    document.querySelector('#ABOUT').style.display = 'block'
}
showAbout()

const hideSections = () => {
    sections.forEach(section => section.style.display = 'none')
}

navLink.forEach(link => {
    link.addEventListener('click', (e) => {
        hideSections()
        document.querySelector(`#${e.target.textContent}`).style.display = 'block';
        e.target.classList.add("header__navbar-links--active");
        link.classList.remove("header__navbar-links--active");
    })
})


editAllFields.addEventListener('click', () => {
    editAllFields.style.visibility = 'hidden';
    form.style.display = 'block';
    sectionInfo.style.display = 'none';
})

cancel.addEventListener('click', () => {
    form.style.display = 'none';
    editAllFields.style.visibility = 'visible';
    sectionInfo.style.display = 'block'
})

save.addEventListener('click', (e) => {
    e.preventDefault();
    const firstname = document.querySelector('#firstname');
    const lastname = document.querySelector('#lastname');
    const website = document.querySelector('#website');
    const phoneNumber = document.querySelector('#phone-number');
    const fieldLocation = document.querySelector('#city');

    const infoNames = document.querySelectorAll('.info-name');
    const infoPhoneNumbers = document.querySelectorAll('.info-phone-number');
    const infoLocations = document.querySelectorAll('.info-location');
    const infoWebsite = document.querySelector('.info-website');

    infoNames.forEach(name => name.textContent = firstname.value + ' ' + lastname.value);
    infoPhoneNumbers.forEach(number => number.textContent = ' ' + phoneNumber.value);
    infoLocations.forEach(location => location.textContent = ' ' + fieldLocation.value);
    infoWebsite.textContent = website.value;

    form.style.display = 'none';
    sectionInfo.style.display = 'block'
    editAllFields.style.visibility = 'visible';
})

const sectionEdit = document.querySelectorAll('.section__icons-edit')
sectionEdit.forEach(edit => {
    edit.addEventListener('click', (e) => {
        document.querySelector('.section__info-tooltip').style.display = 'block';
        const tooltipSave = document.querySelector('#tooltip-save');
        tooltipSave.addEventListener('click', (e) => {
            const value = document.querySelector('#tooltip-input').value;
        })
    })

})

const tooltipCancel = document.querySelector('#tooltip-cancel');
tooltipCancel.addEventListener('click', () => {
    document.querySelector('.section__info-tooltip').style.display = 'none';
})


// swipe navigation on mobile
const pageId = ['ABOUT', 'SETTINGS', 'OPTION1', 'OPTION2', 'OPTION3'];

if(window.innerWidth < 500) {
    sections.forEach(section => {
        section.addEventListener('touchmove', (e) => {
            let start = e.changedTouches[0].clientX;
            let result = window.innerWidth - start;
            result > window.innerWidth/2 ? rightSwipe(e) : leftSwipe(e);
        })
    })
}

const rightSwipe = (e) => {
    if(e.target.offsetParent.id === 'ABOUT') {
        e.target.offsetParent.style.display = 'block';
    } 
    else {
        e.target.style.display = 'none';
        let index = pageId.indexOf(e.target.id)
        let previousPage = pageId[index- 1]
        document.querySelector(`#${previousPage}`).style.display = 'block';
    }
}

const leftSwipe = (e) => {
    if(e.target.offsetParent.id === 'ABOUT') {
        e.target.offsetParent.style.display = 'none';
        document.querySelector('#SETTINGS').style.display = 'block';
    } 
    else {
        if(e.target.id !== 'OPTION3') {
            e.target.style.display = 'none';
            let index = pageId.indexOf(e.target.id)
            let nextPage = pageId[index+ 1]
            document.querySelector(`#${nextPage}`).style.display = 'block';
        }
    }
}
